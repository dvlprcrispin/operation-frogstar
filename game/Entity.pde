abstract class Entity { //<>//
  int health = 3;
  PVector pos;
  PVector aabb;
  // used for the flash when an entity gets hit
  int hitFlashTimer = 0;
  void update() {
    if (health <= 0) {
      state.removeQueue.add(this);
      onDeath();
    }
    if (hitFlashTimer > 0) {
      hitFlashTimer--;
    }
  };
  // i seperate update and draw so that i can make sure everything is properly processed
  // and in the right place before i draw things
  void draw() {
    if (hitFlashTimer > 0) {
      pushStyle();
      fill(255);
      rect(pos.x, pos.y, aabb.x, aabb.y);
      popStyle();
    }
  }
  abstract void collide(Entity e);
  void onDeath() {
    // leaving this implemented but empty so that any classes that don't need to write it don't have to
  };
  boolean isOffscreen() {
    boolean offscreenX = pos.x + aabb.x < 0 || pos.x > pxWidth;
    boolean offscreenY = pos.y + aabb.y < 0 || pos.y > pxHeight;
    return offscreenX || offscreenY;
  }
}

enum PowerupState {
  None, 
    Bullet, 
    Fast, 
    Shield
}

class Player extends Entity {
  final int speed = 2;
  int score = 0;
  int combo = 0;

  PowerupState power = PowerupState.None;
  // how long until the powerup runs out
  int powerupTimer;

  // used for the tongue animation
  Powerup targetPowerup;
  // how many frames the animation has left
  int powerupGrabTimer = 0;

  Player(int x, int y) {
    pos = new PVector(x, y);
    aabb = new PVector(14, 16);
  }

  void update() {
    super.update();

    // movement
    int thisFrameSpeed = power == PowerupState.Fast ? speed * 2 : speed;
    if (keyDown(UP)) {
      pos.y -= thisFrameSpeed;
    } else if (keyDown(DOWN)) {
      pos.y += thisFrameSpeed;
    }

    if (keyDown(LEFT)) {
      pos.x -= thisFrameSpeed;
    } else if (keyDown(RIGHT)) {
      pos.x += thisFrameSpeed;
    }

    if (key(KEY_Z) == keyState.JustPressed) {
      if (power == PowerupState.Bullet) {
        state.addQueue.add(new Bullet(pos.x + 2, pos.y - 9, UP));
        state.addQueue.add(new Bullet(pos.x + 12, pos.y - 9, UP));
      } else {
        state.addQueue.add(new Bullet(pos.x + 6, pos.y - 9, UP));
      }
    }
    if (key(KEY_X) == keyState.JustPressed && targetPowerup == null) {
      eat();
    }

    //don't let the player go off screen
    if (pos.x < 0) {
      pos.x = 0;
    } else if (pos.x + aabb.x > pxWidth) {
      pos.x = pxWidth - aabb.x;
    }

    if (pos.y < 0) {
      pos.y = 0;
    } else if (pos.y + aabb.y > pxHeight) {
      pos.y = pxHeight - aabb.y;
    }

    // if the counter is counting down then we're locked onto a powerup
    if (powerupGrabTimer > 0) {
      // if it's the last possible frame then we eat it
      if (powerupGrabTimer == 1) {
        targetPowerup.onConsume();
        targetPowerup.health = 0;
        targetPowerup = null;
      }
      powerupGrabTimer--;
    }
    powerupTimer--;
    if (power != PowerupState.None && powerupTimer < 0) {
      power = PowerupState.None;
    }
  }

  void onDeath() {
    state.nextState = new EndState(score);
  }

  void draw() {
    image(assets.get("frog"), pos.x, pos.y);

    // tongue
    if (powerupGrabTimer > 0) {
      pushStyle();
      stroke(255, 119, 168);
      strokeWeight(3);
      line(pos.x + 8, pos.y, targetPowerup.pos.x + 4, targetPowerup.pos.y + 4);
      popStyle();
    }

    // shield powerup display
    if (power == PowerupState.Shield) {
      pushStyle();
      fill(95, 205, 228, 128);
      circle(pos.x + 7, pos.y + 7, 16);
      popStyle();
    }

    super.draw();
  }

  void collide(Entity e) {
    if (e instanceof Bullet) {
      if (power == PowerupState.Shield) {
        powerupTimer /= 2;
      } else {
        health--;
      }
      combo = 0;
      hitFlashTimer = 5;
    }
  }

  void eat() {
    Powerup p = null;
    for (Entity e : state.entities) {
      if (e instanceof Powerup && distance(pos, e.pos) < 16 * 4) {
        p = (Powerup) e;
        break;
      }
    }

    if (p != null) {
      targetPowerup = p;
      powerupGrabTimer = 20;
    }
  }

  void addScore(int i) {
    if (combo < 5) {
      combo++;
    }
    if (combo > 1) {
      float factor = 1 + float(combo)/10.0;
      int scoreIncrease = int(float(i) * factor);
      score += scoreIncrease;
      state.addQueue.add(new Text(pos.x + 16, pos.y - 8, scoreIncrease + " - " + combo + "COMBO!"));
    } else {
      score += i;
      state.addQueue.add(new Text(pos.x + 16, pos.y - 8, i + ""));
    }
  }
}

class Text extends Entity {
  String text;
  int countdown = 60;

  Text(float x, float y, String text) {
    this.pos = new PVector(x, y);
    this.aabb = new PVector(0, 0);
    this.text = text;
  }

  void update() {
    countdown--;
    if (countdown <= 0) {
      state.removeQueue.add(this);
    }
  }

  void draw() {
    pushStyle();
    text(text, pos.x, pos.y);
    popStyle();
  }

  void collide(Entity e) {
  }
}


class Bullet extends Entity {
  int direction;
  final float speed = 4;

  Bullet(float x, float y, int direction) {
    // direction should be one of UP, DOWN (which are meant for key codes but they work well here)
    pos = new PVector(x, y);
    aabb = new PVector(2, 4);
    this.direction = direction;
    health = 1;
  }

  void update() {
    super.update();
    if (this.direction == UP) {
      pos.y -= speed;
    } else {
      pos.y += speed;
    }
  }

  void draw() {
    pushStyle();
    fill(255, 199, 168);
    rect(pos.x, pos.y, aabb.x, aabb.y);
    popStyle();
  }

  void collide(Entity e) {
    if (!(e instanceof Bullet || e instanceof Powerup)) {
      health--;
    }
  }
}

abstract class Enemy extends Entity {
  Enemy(int x, int y) {
    pos = new PVector(x, y);
  }
  // I want the enemy to spawn above the screen so I remove the check for if it's above the screen so it dosen't get killed
  boolean isOffscreen() {
    boolean offscreenX = pos.x + aabb.x < 0 || pos.x > pxWidth;
    boolean offscreenY = pos.y > pxHeight;
    return offscreenX || offscreenY;
  }

  void collide(Entity e) {
    if (e instanceof Bullet) {
      health--;
      hitFlashTimer = 5;
    }
  }
}

class FlyEnemy extends Enemy {
  int countdown = 40;
  FlyEnemy(int x, int y) {
    super(x, y);
    aabb = new PVector(12, 16);
    health = 2;
  }

  void update() {
    super.update();

    pos.y += 0.4;
    countdown--;

    // basic "AI" -  just "if player below us and we haven't shot recently then shoot"
    boolean playerBelowEnemy = state.player.pos.y > pos.y + aabb.y;
    boolean playerSameX = state.player.pos.x + state.player.aabb.x > pos.x && pos.x + aabb.x > state.player.pos.x;
    if (countdown <= 0 && playerBelowEnemy && playerSameX) {
      state.addQueue.add(new Bullet(pos.x + 6, pos.y + aabb.y + 1, DOWN));
      countdown = 90;
    }
  }

  void draw() {
    image(assets.get("fly"), pos.x, pos.y);
    super.draw();
  }

  void onDeath() {
    state.player.addScore(40);
    state.spawnPowerup(add(pos, 4));
  }
}

class BeeEnemy extends Enemy {
  int countdown = 40;
  int centreX;
  float t = 0;
  BeeEnemy(int x, int y) {
    super(x, y);
    aabb = new PVector(16, 13);
    health = 1;
    centreX = x;
  }

  void update() {
    super.update();

    // move downwards in a siney pattern
    pos.y += 0.6;
    pos.x = centreX + sin(t) * 25;
    t += TAU/60;
    countdown--;

    if (countdown <= 0) {
      state.addQueue.add(new Bullet(pos.x + 6, pos.y + aabb.y + 1, DOWN));
      countdown = 40;
    }
  }

  void draw() {
    image(assets.get("bee"), pos.x, pos.y);
    super.draw();
  }

  void onDeath() {
    state.player.addScore(60);
    state.spawnPowerup(add(pos, 4));
  }
}

class MothEnemy extends Enemy {
  int countdown = 100;

  MothEnemy(int x, int y) {
    super(x, y);
    aabb = new PVector(16, 9);
    health = 3;
  }

  void update() {
    super.update();

    // move towards the player horizontally
    pos.y += 0.4;
    if (state.player.pos.x < pos.x - 10) {
      pos.x -= 1;
    } else if (state.player.pos.x > pos.x + 10) {
      pos.x += 1;
    }

    countdown--;

    if (countdown <= 0) {
      state.addQueue.add(new Bullet(pos.x + 6, pos.y + aabb.y + 1, DOWN));
      countdown = 100;
    }
  }

  void draw() {
    image(assets.get("moth"), pos.x, pos.y);
    super.draw();
  }

  void onDeath() {
    state.player.addScore(60);
    state.spawnPowerup(add(pos, 4));
  }
}

class BeetleEnemy extends Enemy {
  int countdown = 100;

  BeetleEnemy(int x, int y) {
    super(x, y);
    aabb = new PVector(12, 15);
    health = 4;
  }

  void update() {
    super.update();


    // just march downwards
    pos.y += 0.3;

    countdown--;

    if (countdown <= 0) {
      state.addQueue.add(new Bullet(pos.x + 6, pos.y + aabb.y + 1, DOWN));
      countdown = 100;
    }
  }

  void draw() {
    image(assets.get("beetle"), pos.x, pos.y);
    super.draw();
  }

  void onDeath() {
    state.player.addScore(60);
    state.spawnPowerup(add(pos, 4));
  }
}

abstract class Powerup extends Entity {
  abstract void onConsume();
}

class ScorePowerup extends Powerup {

  ScorePowerup(PVector p) {
    pos = p;
    aabb = new PVector(8, 8);
    health = 1;
  }

  void draw() {
    image(assets.get("bug-shell"), pos.x, pos.y);
  }

  void collide(Entity e) {
  }

  void onConsume() {
    state.player.addScore(100);
  }
}

class HealthPowerup extends Powerup {

  HealthPowerup(PVector p) {
    pos = p;
    aabb = new PVector(8, 8);
    health = 1;
  }

  void draw() {
    image(assets.get("frog-icon"), pos.x, pos.y);
  }

  void collide(Entity e) {
  }

  void onConsume() {
    if (state.player.health < 5) {
      state.player.health++;
    }
  }
}

class BulletPowerup extends Powerup {

  BulletPowerup(PVector p) {
    pos = p;
    aabb = new PVector(8, 8);
    health = 1;
  }

  void draw() {
    image(assets.get("bullet-icon"), pos.x, pos.y);
  }

  void collide(Entity e) {
  }

  void onConsume() {
    if (state.player.power == PowerupState.None) {
      state.player.power = PowerupState.Bullet;
      state.player.powerupTimer = 60 * 5;
      state.player.addScore(70);
    }
  }
}

class FastPowerup extends Powerup {

  FastPowerup(PVector p) {
    pos = p;
    aabb = new PVector(8, 8);
    health = 1;
  }

  void draw() {
    image(assets.get("fast-icon"), pos.x, pos.y);
  }

  void collide(Entity e) {
  }

  void onConsume() {
    if (state.player.power == PowerupState.None) {
      state.player.power = PowerupState.Fast;
      state.player.powerupTimer = 60 * 5;
      state.player.addScore(70);
    }
  }
}

class ShieldPowerup extends Powerup {

  ShieldPowerup(PVector p) {
    pos = p;
    aabb = new PVector(8, 8);
    health = 1;
  }

  void draw() {
    image(assets.get("shield-icon"), pos.x, pos.y);
  }

  void collide(Entity e) {
  }

  void onConsume() {
    if (state.player.power == PowerupState.None) {
      state.player.power = PowerupState.Shield;
      state.player.powerupTimer = 60 * 5;
      state.player.addScore(70);
    }
  }
}
