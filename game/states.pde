abstract class State {
  State() {
    // clear the key tracker out so that you don't have keypresses crossing the state boundary
    keyTracker.clear();
  }

  // how far through the wipe between states it is
  float stateTransitionCounter = 30;

  void draw() {
    if (nextState != null) {
      // hold the screen for 10f after it's covered
      if (stateTransitionCounter <= -10) {
        state = nextState;
      }
      pushStyle();
      fill(131, 118, 156);
      rect(0, 0, pxWidth, pxHeight * lerp(1, 0, stateTransitionCounter/30));
      popStyle();
      stateTransitionCounter--;
      return;
    }
  }
  // this is a bit cheeky - things like enemies need to have access to the player and the queues
  // but not all states have entities/a player (eg. menu states) so it'll be null then
  // though luckily those states also don't have things that need to access the player
  Player player;

  ArrayList<Entity> entities;
  // you can't modify a List while it's being iterated over
  // so make a list of new entities and {add, remove} them after all updates are completed
  ArrayList<Entity> addQueue;
  ArrayList<Entity> removeQueue;

  // if you try and update the state directly while you're iterating through entites
  // the next thing to update tries to get something from the state that isn't there anymore and null pointer exceptions
  // so queue it up and only change it at the end of the frame
  State nextState;

  // again, something only gameplay states need but it makes typing a whole lot easier 
  void spawnPowerup(PVector pos) {
  };
}

abstract class PlayState extends State {
  int nextWaveCountdown = 240;

  PlayState() {
    super();
    player = new Player((tileSize * tileWidth)/2-8, 8*(tileSize * tileHeight)/9);

    entities = new ArrayList<Entity>();
    addQueue = new ArrayList<Entity>();
    removeQueue = new ArrayList<Entity>();

    entities.add(player);
  }

  void draw() {

    nextWaveCountdown--;
    if (nextWaveCountdown < 0) {
      spawnEnemies();
      nextWaveCountdown = 240;
    }

    collide();
    for (Entity e : entities) {
      e.update();
    }

    removeOffscreen();

    for (Entity e : addQueue) {
      entities.add(e);
    }
    addQueue.clear();

    for (Entity e : removeQueue) {
      entities.remove(e);
    }
    removeQueue.clear();

    preDrawProcess();

    updateKeys();

    drawStarfield();
    for (Entity e : entities) {
      e.draw();
    }
    drawUI();

    super.draw();
  }

  abstract void spawnEnemies();
  abstract void preDrawProcess();

  protected PVector[] enemyLocations() {
    float choice = random(1);
    if (choice < 0.5) {
      return new PVector[] {new PVector((int) random(pxWidth-16), -32)};
    } else if (choice < 0.75) {
      return new PVector[] {new PVector((int) random(pxWidth/2), -32), 
        new PVector((int) random(pxWidth/2, pxWidth-16), -32)};
    } else {
      return new PVector[] {new PVector((pxWidth)/3-8, -32), 
        new PVector((pxWidth)/2-8, -64), 
        new PVector(2*(pxWidth)/3-8, -32)};
    }
  }


  // reasonably simple many-many aabb collision
  void collide() {
    for (Entity a : entities) {
      for (Entity b : entities) {
        // we already know that an object is in the same position as itself
        if (a == b) {
          continue;
        }
        boolean xCollision = a.pos.x > b.pos.x + b.aabb.x || b.pos.x > a.pos.x + a.aabb.x;
        boolean yCollision = a.pos.y > b.pos.y + b.aabb.y || b.pos.y > a.pos.y + a.aabb.y;
        if (!(xCollision || yCollision)) {
          a.collide(b);
        }
      }
    }
  }

  // remove all entities that are offscreen
  // i do this so that i only process entities that can actually affect things
  // i've got a bunch of O(entites) processes (and collision is O(entites^2)) so i wanna keep entities small
  void removeOffscreen() {
    for (Entity e : entities) {
      if (e.isOffscreen()) {
        removeQueue.add(e);
      }
    }
  }

  void drawUI() {
    pushStyle();
    textFont(font, 9);
    text(String.format("SCORE: %06d", player.score), 8, 14);
    fill(0, 135, 81);
    // fill health icons in right to left
    for (int i = 0; i < player.health; i++) {
      image(assets.get("frog-icon"), 208 - (i * 16), 8);
    }
    popStyle();
  }

  void drawWireframes() {
    pushStyle();
    stroke(0, 228, 54);
    strokeWeight(1);
    noFill();
    for (Entity e : entities) {
      rect(e.pos.x, e.pos.y, e.aabb.x, e.aabb.y);
    }
    popStyle();
  }
}

class TestState extends PlayState {
  TestState() {
    super();
    entities.add(new BeetleEnemy((tileSize * tileWidth)/2-8, -32));
  }

  void preDrawProcess() {
  }
  void spawnEnemies() {
  }
  void spawnPowerup() {
  }
}

class Level1State extends PlayState {
  Level1State() {
    super();
    entities.add(new FlyEnemy((tileSize * tileWidth)/2-8, -32));
  }

  void spawnEnemies() {
    PVector locations[] = enemyLocations();
    for (PVector p : locations) {
      addQueue.add(new FlyEnemy(int(p.x), int(p.y)));
    }
  }

  void spawnPowerup(PVector pos) {
    float chance = random(1);
    if (chance < 0.9) {
      if (player.health < 3 && chance < 0.75 ||
        player.health > 3 && player.health < 5 && chance < 0.4) {
        addQueue.add(new HealthPowerup(pos));
      } else {
        addQueue.add(new ScorePowerup(pos));
      }
    }
  }

  void preDrawProcess() {
    if (player.score >= 1000) {
      nextState = new MidState(player.score);
    }
  }
}

class Level2State extends PlayState {
  Level2State(int score) {
    super();
    player.score = score;
    entities.add(new FlyEnemy((tileSize * tileWidth)/2-8, -32));
  }

  void spawnEnemies() {
    int choice = int(random(4));
    PVector locations[] = enemyLocations();
    for (PVector p : locations) {
      if (choice == 0) {
        addQueue.add(new FlyEnemy(int(p.x), int(p.y)));
      } else if (choice == 1) {
        addQueue.add(new BeeEnemy(int(p.x), int(p.y)));
      } else if (choice == 2) {
        addQueue.add(new MothEnemy(int(p.x), int(p.y)));
        break; // I only want one moth enemy to spawn at once so end after the first is spawned
      } else if (choice == 3) {
        addQueue.add(new BeetleEnemy(int(p.x), int(p.y)));
      }
    }
  }

  void spawnPowerup(PVector pos) {
    float chance = random(1);
    if (chance < 0.9) {
      if (player.health < 3 && chance < 0.75 ||
        player.health > 3 && player.health < 5 && chance < 0.4) {
        addQueue.add(new HealthPowerup(pos));
      } else if (chance < 0.5) {
        int powerup = int(random(3));
        if (powerup == 0) {
          addQueue.add(new FastPowerup(pos));
        } else if (powerup == 1) {
          addQueue.add(new BulletPowerup(pos));
        } else {
          addQueue.add(new ShieldPowerup(pos));
        }
      } else {
        addQueue.add(new ScorePowerup(pos));
      }
    }
  }

  void preDrawProcess() {
  }
}


class StartState extends State {

  void draw() {
    drawStarfield();

    pushStyle();
    textAlign(CENTER); // grr american spelling
    textFont(font, 18);
    text("OPERATION:\nFROGSTAR", tileSize * 7, tileSize * 2);
    text("Z TO START", tileSize * 7, tileSize * 15);

    textFont(font, 9);
    text("Z SHOOTS X EATS\nNEXT LEVEL AT 1000", tileSize * 7, tileSize * 10);
    popStyle();

    if (keyDown(KEY_Z)) {
      nextState = new Level1State();
      //nextState = new TestState();
    }
    super.draw();
  }
}

class MidState extends State {
  int score;
  int countdown = 60 * 5;
  MidState(int score) {
    super();
    // take score so we can pass it onto the player in the next stage
    this.score = score;
  }

  void draw() {
    drawStarfield();

    pushStyle();
    textFont(font, 18);
    textAlign(CENTER);

    text("FROG WORLD SAVED", tileSize * 7, tileSize * 2);
    text("BUT THE FIGHT\nCONTINUES", tileSize * 7, tileSize * 10);
    text(String.format("CONTINUE IN %d", (countdown / 60)+1), tileSize * 7, tileSize * 15);
    popStyle();

    countdown--;
    if (countdown <= 0) {
      nextState = new Level2State(score);
    }
    super.draw();
  }
}

class EndState extends State {
  int score;
  int countdown = 60 * 5;
  EndState(int score) {
    super();
    this.score = score;
  }

  void draw() {
    drawStarfield();

    pushStyle();
    textFont(font, 18);
    textAlign(CENTER);
    text("YOU LOSE", tileSize * 7, tileSize * 2);
    text(String.format("SCORE: %06d", score), tileSize * 7, tileSize * 10);
    text(String.format("CONTINUE IN %d", (countdown / 60)+1), tileSize * 7, tileSize * 15);
    popStyle();

    countdown--;
    if (countdown <= 0) {
      nextState = new StartState();
    }
    super.draw();
  }
}
