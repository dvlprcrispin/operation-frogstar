// this keeps track of what state the keys are in
// and lets them be queried within update()s without having to go throught the key*() functions
enum keyState {
  JustPressed, 
    Pressed, 
    JustReleased, 
    Released
}

// constants for the keys I'm using that don't already have them
final int KEY_Z = 90;
final int KEY_X = 88;

HashMap<Integer, keyState> keyTracker = new HashMap<Integer, keyState>(); 

void keyPressed() {
  keyTracker.put(keyCode, keyState.JustPressed);
  // bit of debug on spacebar press
  if (key == ' ' && state instanceof PlayState) {
    state.player.addScore(510);
    state.addQueue.add(new ShieldPowerup(new PVector(32, 12 * tileSize)));
  }
}

void keyReleased() {
  keyTracker.put(keyCode, keyState.JustReleased);
}

// each frame, update from rising/falling edge to settled state
void updateKeys() {
  for (Integer i : keyTracker.keySet()) {
    if (keyTracker.get(i) == keyState.JustPressed) {
      keyTracker.put(i, keyState.Pressed);
    } else if (keyTracker.get(i) == keyState.JustReleased) {
      keyTracker.put(i, keyState.Released);
    }
  }
}

keyState key(int i) {
  // if a key isn't in the tracker then that means it hasn't been pressed
  return keyTracker.getOrDefault(i, keyState.Released);
}

// just to make some other code a bit more ergonomic
boolean keyDown(int i) {
  keyState status = key(i);
  return (status == keyState.Pressed || status == keyState.JustPressed);
}
