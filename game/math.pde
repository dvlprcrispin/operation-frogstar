// Adds a scalar factor to both components of a 2d vector
PVector add(PVector p, int n) {
  return new PVector(p.x + n, p.y + n);
}

// The euclidean/straightline distance between two 2d vectors
double distance(PVector p, PVector q) {
  double inner = Math.pow(q.x - p.x, 2) + Math.pow(q.y - p.y, 2);
  return Math.sqrt(inner);
}
