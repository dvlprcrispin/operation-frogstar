// these determine how big the screen is in terms of multiples of the art tile size
// giving us a (224 x 288) working resoloution
final int tileSize = 16;
final int tileWidth = 14;
final int tileHeight = 18;

// this is the working width/height of the screen
final int pxWidth = tileWidth * tileSize;
final int pxHeight = tileHeight * tileSize;

// this is how much the canvas is scaled up by for display
final int scaleFactor = 3;

// need to use settings() if there are variables in the call to size()
void settings() {
  size(pxWidth * scaleFactor, pxHeight * scaleFactor);
  noSmooth();
}

// i load all the assets once and then get them at draw time from this map
HashMap<String, PImage> assets = new HashMap<String, PImage>();
PFont font;

State state;

void setup() {  
  state = new StartState();
  surface.setTitle("Operation: FROGSTAR");
  assets.put("noise", loadImage("noise.png"));

  assets.put("frog", loadImage("froggy.png"));
  assets.put("fly", loadImage("fly.png"));
  assets.put("bee", loadImage("bee.png"));
  assets.put("moth", loadImage("moth.png"));
  assets.put("beetle", loadImage("beetle.png"));

  assets.put("bullet-icon", loadImage("bullet-icon.png"));
  assets.put("fast-icon", loadImage("fast-icon.png"));
  assets.put("shield-icon", loadImage("shield-icon.png"));
  assets.put("frog-icon", loadImage("frog-icon.png"));
  assets.put("bug-shell", loadImage("bug-shell.png"));
  font = createFont("Courneuf-Regular.ttf", 9, false);
}

void draw() {
  // scaling here lets me program at pixel art scale but render at a viewable size
  scale(scaleFactor);
  noStroke();

  state.draw();
}

// how far through the noise texture the starfield is
float yScroll = 0;

// i've pregenerate a noise texture and then sample from that - if it's above a certain brightness level then i draw a star there
void drawStarfield() {
  pushStyle();
  background(29, 43, 83);
  stroke(194, 195, 199);
  PImage noise = assets.get("noise");
  for (int x = 0; x < pxWidth; x++) {
    for (int y = 0; y < pxHeight; y++) {
      // all the channels are the same so i just sample from red
      if (red(noise.get(x, int (y - yScroll) % noise.height)) > 253) {
        point(x, y);
      }
    }
  }
  yScroll = (yScroll - 0.5) ;
  popStyle();
}
