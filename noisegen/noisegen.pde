size(224, 1440);
print(width, height);

PImage i = createImage(width, height, RGB);

for (int x = 0; x < width; x++) {
  for (int y = 0; y < height; y++) {
    i.set(x, y, color(random(255)));
  }
}

image(i, 0, 0);

i.save("noise.png");
