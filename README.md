# Operation: FROGSTAR
This is the game I wrote for my CGRA151 final project.

The Processing sketch is stored in the `game` folder, along with the original aseprite art files and a [glint](https://gitlab.com/dvlprcrispin/glint) build script. I've also included the `noisegen` sketch that I used to create the noise texture for the starfield.

I've released the code under the GPL and the assets under CC BY-NC-SA, and the gane uses the font [Courneuf](https://fontlibrary.org/en/font/courneuf-family) released under the SIL Open Font License - see `LICENSES` for more details
